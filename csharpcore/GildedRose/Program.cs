﻿using GildedRoseKata.Models;

namespace GildedRoseKata
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var qualityUpdateStrategyFactory = new QualityUpdateStrategyFactory();
            var gildedRose = new GildedRose(qualityUpdateStrategyFactory);

            var logger = new ConsoleLogger();
            var itemManager = new ItemManager(logger, gildedRose);

            logger.Log("OMGHAI!");
            var items = new ItemDeposit().GetItems();
            itemManager.RunQualityChangePredictions(items);
        }
    }
}
