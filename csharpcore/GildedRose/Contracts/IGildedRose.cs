﻿using System.Collections.Generic;

namespace GildedRoseKata.Contracts
{
    public interface IGildedRose
    {
        void UpdateQuality(IList<Item> items);
    }
}