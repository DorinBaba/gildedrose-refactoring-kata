﻿namespace GildedRoseKata.Contracts
{
    public interface IQualityUpdateStrategyFactory
    {
        public IQualityUpdateStrategy GetQualityUpdateStrategy(string itemName);
    }
}
