﻿namespace GildedRoseKata.Contracts
{
    public interface IQualityUpdateStrategy
    {
        public void UpdateQuality(Item item);
    }
}
