﻿namespace GildedRoseKata.Contracts
{
    interface ILogger
    {
        public void Log(string message);
    }
}
