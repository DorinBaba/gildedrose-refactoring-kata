﻿using GildedRoseKata.Contracts;
using System;

namespace GildedRoseKata.Models
{
    class ConsoleLogger : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
