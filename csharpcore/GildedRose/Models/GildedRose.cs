﻿using GildedRoseKata.Contracts;
using System.Collections.Generic;

namespace GildedRoseKata.Models
{
    public class GildedRose : IGildedRose
    {
        readonly IQualityUpdateStrategyFactory _strategyFactory;

        public GildedRose(IQualityUpdateStrategyFactory strategyFactory)
        {
            _strategyFactory = strategyFactory;
        }

        public void UpdateQuality(IList<Item> items)
        {
            for (var i = 0; i < items.Count; i++)
            {
                var qualityUpdateStrategy = _strategyFactory.GetQualityUpdateStrategy(items[i].Name);
                qualityUpdateStrategy.UpdateQuality(items[i]);
            }
        }
    }
}
