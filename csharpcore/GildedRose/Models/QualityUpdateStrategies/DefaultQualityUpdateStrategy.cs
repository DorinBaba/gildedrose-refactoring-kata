﻿using GildedRoseKata.Contracts;

namespace GildedRoseKata.Models.QualityUpdateStrategies
{
    class DefaultQualityUpdateStrategy : IQualityUpdateStrategy
    {
        public void UpdateQuality(Item item)
        {
            --item.SellIn;
            --item.Quality;

            if (item.SellIn < 0)
                --item.Quality;

            if (item.Quality < 0)
                item.Quality = 0;
        }
    }
}
