﻿using GildedRoseKata.Contracts;

namespace GildedRoseKata.Models.QualityUpdateStrategies
{
    class BackstagePassesQualityUpdateStrategy : IQualityUpdateStrategy
    {
        public void UpdateQuality(Item item)
        {
            --item.SellIn;
            ++item.Quality;

            if(item.SellIn < 0)
            {
                item.Quality = 0;
                return;
            }

            if (item.SellIn < 10)
            {
                ++item.Quality;
            }

            if (item.SellIn < 5)
            {
                ++item.Quality;
            }

            if (item.Quality > 50)
            {
                item.Quality = 50;
            }
        }
    }
}
