﻿using GildedRoseKata.Contracts;

namespace GildedRoseKata.Models.QualityUpdateStrategies
{
    class ConjuredQualityUpdateStrategy : IQualityUpdateStrategy
    {
        public void UpdateQuality(Item item)
        {
            --item.SellIn;
            item.Quality -= 2;

            if (item.Quality < 0)
                item.Quality = 0;
        }
    }
}
