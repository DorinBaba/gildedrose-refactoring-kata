﻿using GildedRoseKata.Contracts;

namespace GildedRoseKata.Models.QualityUpdateStrategies
{
    class AgedBrieQualityUpdateStrategy : IQualityUpdateStrategy
    {
        public void UpdateQuality(Item item)
        {
            --item.SellIn;
            ++item.Quality;

            if (item.SellIn < 0)
                ++item.Quality; // Just to pass the tests!?

            if (item.Quality > 50)
                item.Quality = 50;
        }
    }
}
