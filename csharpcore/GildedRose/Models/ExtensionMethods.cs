﻿namespace GildedRoseKata.Models
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Checks CASE INSESITIVELY if the subsequence is contained within sequence
        /// </summary>
        /// <param name="sequence"></param>
        /// <param name="subsequence"></param>
        /// <returns></returns>
        public static bool IsMatch(this string sequence, string subsequence)
        {
            return sequence.ToLower().IndexOf(subsequence.ToLower()) > -1;
        }
    }
}
