﻿using GildedRoseKata.Contracts;
using System.Collections.Generic;

namespace GildedRoseKata.Models
{
    class ItemManager
    {
        readonly IGildedRose _gildedRose;
        readonly ILogger _logger;

        public ItemManager(ILogger logger, IGildedRose gildedRose)
        {
            _logger = logger;
            _gildedRose = gildedRose;
        }

        /// <summary>
        /// Displays quality changes for each item for the next 31 days
        /// </summary>
        public void RunQualityChangePredictions(IList<Item> items)
        {
            for (var i = 0; i < 31; i++)
            {
                DisplayItemQualityOfDay(i, items);
                _gildedRose.UpdateQuality(items);
            }
        }

        private void DisplayItemQualityOfDay(int day, IList<Item> items)
        {
            _logger.Log("-------- day " + day + " --------");
            _logger.Log("name, sellIn, quality");

            for (var j = 0; j < items.Count; j++)
            {
                _logger.Log(items[j].Name + ", " + items[j].SellIn + ", " + items[j].Quality);
            }

            _logger.Log("");
        }
    }
}
