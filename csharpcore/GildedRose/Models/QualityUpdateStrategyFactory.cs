﻿using GildedRoseKata.Contracts;
using GildedRoseKata.Models.QualityUpdateStrategies;
using System;
using System.Collections.Generic;

namespace GildedRoseKata.Models
{
    class QualityUpdateStrategyFactory : IQualityUpdateStrategyFactory
    {
        Dictionary<string, IQualityUpdateStrategy> strategies;

        public QualityUpdateStrategyFactory()
        {
            strategies = new Dictionary<string, IQualityUpdateStrategy>();
        }

        public IQualityUpdateStrategy GetQualityUpdateStrategy(string itemName)
        {
            if (itemName.IsMatch("Aged Brie"))
                return GetOrAdd<AgedBrieQualityUpdateStrategy>("Aged Brie");

            if(itemName.IsMatch("Sulfuras"))
                return GetOrAdd<SulfurasQualityUpdateStrategy>("Sulfuras");

            if(itemName.IsMatch("Backstage passes"))
                return GetOrAdd<BackstagePassesQualityUpdateStrategy>("Backstage passes");

            if (itemName.IsMatch("Conjured"))
                return GetOrAdd<ConjuredQualityUpdateStrategy>("Conjured");

            return GetOrAdd<DefaultQualityUpdateStrategy>("Default"); ;
        }

        private IQualityUpdateStrategy GetOrAdd<TStrategy>(string key) 
            where TStrategy : IQualityUpdateStrategy
        {
            IQualityUpdateStrategy strategy;

            if (strategies.TryGetValue(key, out strategy))
                return strategy;

            strategy = (IQualityUpdateStrategy)Activator.CreateInstance(typeof(TStrategy));
            strategies.Add(key, strategy);
            return strategy;
        }
    }
}
